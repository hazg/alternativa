import './modules';

import jQuery from 'jquery';
import Slick from 'slick-carousel';
import Bootstrap from 'bootstrap';
import jQueryScrollbar from 'jquery.scrollbar';

window.jQuery = window.$ = jQuery

$(function () {

  /* SLICK SLIDER*/
  // TODO: Turn header slider on
  /*$('.header-slick').slick({
    variableWidth: true
  });*/

  /* EVENTS */
  $('.events-slick').slick({
    // variableWidth: true,
    // centerMode: true,
    centerPadding: '0',
    dots: true,
    slidesToShow: 3,
    adaptiveHeight: true,
    arrows: false,

    responsive: [
      {
        breakpoint: 720,
        settings: {
          slidesToShow: 1,
          centerMode: true,
        }
      },
    ],
  });

  /* AGREE CHECKBOX */
  $('.abc-checkbox input').change(function(){
    var submit = $(this).parents('form').find('[type=submit]');
    if($(this).is(':checked')){
      submit.prop('disabled', '');
    }else{
      submit.prop('disabled', 'disabled');
    }
  });

  /* GIFTS */
  $('.gifts-cards').each(function () {
    $('input[type=checkbox]', this).change(function () {
      // Uncheck all
      $('.gifts-cards input[type=checkbox]').not(this).each(function () {
        $(this).prop('checked', false);
        $(this).parents('.card').removeClass('active');
      });
      // TODO: Make it functional
      if ($(this).is(':checked')) {
        // CHECK GIFT
        $(this).parents('.card').addClass('active');
      } else {
        return false;
      }
    });
  });

  /* SCROLL CONTAINER */
  $(document).ready(function () {
    $('.scrollbar-outer').scrollbar();
  });



  /*$(document).ready(function () {

    //Check to see if the window is top if not then display button
    $(window).scroll(function () {
      if ($(this).scrollTop() > 100) {
        $('.scroll-to-top').fadeIn();
      } else {
        $('.scroll-to-top').fadeOut();
      }
    });

    //Click event to scroll to top
    $('.scroll-to-top').click(function () {
      $('html, body').animate({
        scrollTop: 0
      }, 800);
      return false;
    });

  });*/
});
