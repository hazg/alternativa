module.exports = {
  html        : true,
  images      : true,
  fonts       : true,
  static      : true,
  svgSprite   : true,
  ghPages     : true,
  stylesheets : true,

/*  module: {
    test: /\.css/,
    use: [
      { loader: 'style-loader', options: { sourceMap: true } },
      { loader: 'css-loader', options: { sourceMap: true } },
      { loader: 'postcss-loader', options: { sourceMap: true } },
      { loader: 'sass-loader', options: { sourceMap: true } }
    ]
  },
*/
  loaders: [
  {
    test: /\.css$/,
    use: ['style-loader', 'css-loader' ]
  },
  {
    test: /\.(jpe?g|png|ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/,
    use: 'base64-inline-loader'
  }],
  plugins: ['base64-inline-loader'],
  development: {
    loaders: [
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader' ]
      },

      /*{test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "url-loader?limit=100000000000"},
      {test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "url-loader?limit=10000000000"},
      {test: /\.(png|jpg|gif|jpeg)$/, loader: "url-loader?limit=100000000"},
      {test: /\.svg/, loader: "svg-url-loader" }*/
      {
        test: /\.(jpe?g|png|ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/,
        use: 'base64-inline-loader'
      }

    ]
  },
  javascripts: {
    loaders: [
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader' ]
      },
      {
        test: /\.(jpe?g|png|ttf|eot|svg|woff(2)?)(\?[a-z0-9=&.]+)?$/,
        use: 'base64-inline-loader'
      }],
    entry: {
      // files paths are relative to
      // javascripts.dest in path-config.json
      app: ["./app.js"]
    },

    provide: {
      jQuery: "jquery",
      Slick: "slick-carousel",
      Bootstrap: "bootstrap",
      jQueryScrollbar: 'jquery.scrollbar'
    }

  },

  browserSync: {
    server: {
      // should match `dest` in
      // path-config.json
      baseDir: 'public'
    }
  },

  production: {
    rev: false
  }
}
